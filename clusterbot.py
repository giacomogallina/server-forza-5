#!/usr/bin/env python3
import random

while True:
    s = input()
    rows = [int(r) for r in s.split()]
    N = len(rows)//2
    board = [rows[:N], rows[N:]]
    pieces = [board[0][r] | board[1][r] for r in range(N)]

    close_spots = []
    for r in range(N):
        for c in range(N):
            if pieces[r] & (1 << c) == 0:
                if ((pieces[r] << 1) | (pieces[r] >> 1)) & (1 << c) != 0:
                    close_spots.append((r, c))
                elif r > 0 and pieces[r-1] & (1 << c) != 0:
                    close_spots.append((r, c))
                elif r < N-1 and pieces[r+1] & (1 << c) != 0:
                    close_spots.append((r, c))

    if len(close_spots) > 0:
        r, c = close_spots[random.randint(0, len(close_spots)-1)]
    else:
        r, c = N//2, N//2
    print(r, c)
