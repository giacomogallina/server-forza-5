let
  pkgs = import <nixpkgs> {};
in pkgs.mkShell {
  buildInputs = [
    pkgs.python310
    pkgs.python310Packages.pygame
  ];
}
