#!/usr/bin/env python3

import sys, argparse, queue, time
from subprocess import Popen, PIPE



parser = argparse.ArgumentParser(description='Programma per disputare una partita di Forza 5 tra bot e/o umani')
parser.add_argument("players", help="/path/to/bot, human", nargs=2, metavar="PLAYER")
parser.add_argument("--gui", action="store_true", help="Obbligatorio se almeno uno dei giocatori è umano. Richiede pygame")
parser.add_argument("--size", type=int, default=64, help="Dimensione della griglia di gioco (default = %(default)s)")
options = parser.parse_args()

if "human" in options.players and not options.gui:
    print("Errore: almeno uno dei giocatori è umano, serve il flag --gui\n")
    parser.print_help()
    sys.exit(1)



players = []
for player in options.players:
    if player == "human":
        players.append("human")
    else:
        players.append(Popen([player], stdin=PIPE, stdout=PIPE, encoding="utf8", shell=True))

N = options.size
cp = 0
board = [[0]*N, [0]*N]
moves_queue = queue.SimpleQueue()
human_is_playing = False



def check_winner():
    for p in range(2):
        for r in range(N):
            if board[p][r] & (board[p][r] << 1) & (board[p][r] << 2) & (board[p][r] << 3) & (board[p][r] << 4):
                return True
        for r in range(N-4):
            if board[p][r] & (board[p][r+1] << 1) & (board[p][r+2] << 2) & (board[p][r+3] << 3) & (board[p][r+4] << 4):
                return True
            if board[p][r] & board[p][r+1] & board[p][r+2] & board[p][r+3] & board[p][r+4]:
                return True
            if board[p][r] & (board[p][r+1] >> 1) & (board[p][r+2] >> 2) & (board[p][r+3] >> 3) & (board[p][r+4] >> 4):
                return True
    return False



def pretty_print():
    for r in range(N//2):
        print(" ", end="")
        for c in range(N):
            bg = 40
            if board[0][2*r] & (1 << c) != 0:
                bg = 41
            elif board[1][2*r] & (1 << c) != 0:
                bg = 43
            fg = 30
            if board[0][2*r+1] & (1 << c) != 0:
                fg = 31
            elif board[1][2*r+1] & (1 << c) != 0:
                fg = 33
            print(f"\x1b[{bg};{fg}m▄", end="")
        print("\x1b[0m")
    if N % 2 == 1:
        print(" ", end="")
        for c in range(N):
            fg = 30
            if board[0][-1] & (1 << c) != 0:
                fg = 31
            elif board[1][-1] & (1 << c) != 0:
                fg = 33
            print(f"\x1b[{fg}m▀", end="")
        print("\x1b[0m")
    print()



def main():
    global cp, human_is_playing

    for turn in range(N**2):
        print(f"Turn #{turn}, player \x1b[30;{41+2*cp}m #{cp} \x1b[0m plays\n")
        if not options.gui:
            pretty_print()

        if players[cp] == "human":
            human_is_playing = True
            r, c = moves_queue.get()
        else:
            players[cp].stdin.write(" ".join(str(n) for n in board[cp]) + " " + " ".join(str(n) for n in board[1 - cp]) + "\n")
            players[cp].stdin.flush()
            m = players[cp].stdout.readline().strip()
            r, c = [int(s) for s in m.split()]

        if r < 0 or c < 0 or r >= N or c >= N or board[cp][r] & (1 << c) != 0:
            raise Exception(f"illegal move at turn {turn}: ({r}, {c}) by player {cp}")

        board[cp][r] += (1 << c)

        print(f"Choosen move: ({r}, {c})\n\n\n")
        if check_winner():
            print(f"Player \x1b[30;{41+2*cp}m #{cp} \x1b[0m won!\n")
            break
        cp = 1 - cp
    else:
        print("Draw!\n")

    if not options.gui:
        pretty_print()



def quit():
    for player in players:
        if player != "human":
            player.terminate()
    sys.exit()



if not options.gui:
    main()
    quit()
else:
    import os, threading

    threading.Thread(target=main, daemon=True).start()
    os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
    import pygame

    pygame.init()

    black = 0, 0, 0
    white = 200, 200, 200
    blue = 10, 100, 255
    dark_grey = 45, 40, 40
    grey = 55, 50, 50
    yellow = 245, 194, 17
    red = 192, 28, 40

    size = 64*15
    screen = pygame.display.set_mode([size, size])
    R = size / (2*N)
    font = pygame.font.Font(pygame.font.get_default_font(), 20)

    while True:
        time.sleep(0.01)

        clicked = False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit()
            elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                clicked = True

        screen.fill(dark_grey)

        x, y = pygame.mouse.get_pos()
        text = None

        for r in range(N):
            for c in range(N):
                if (x - R*(2*c+1))**2 + (y - R*(2*r+1))**2 <= R**2:
                    text = f"{r}, {c}"
                color = grey
                if board[0][r] & (1 << c) != 0:
                    color = red
                elif board[1][r] & (1 << c) != 0:
                    color = yellow
                elif human_is_playing and (x - R*(2*c+1))**2 + (y - R*(2*r+1))**2 <= R**2:
                        color = [red, yellow][cp]
                        if clicked:
                            human_is_playing = False
                            moves_queue.put([r, c])

                pygame.draw.circle(screen, color, (R*(2*c+1), R*(2*r+1)), R)

        if text is not None:
            screen.blit(font.render(text, True, blue), dest = (x+20, y+10))

        pygame.display.flip()

