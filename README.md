## Che cos'è qusto
`forza5.py` è un server che permette di disputare una partita di Forza 5 tra bot e/o umani, dotato
opzionalmente di un'interfaccia grafica per far giocare quest'ultimi.
`randombot.py` e `clusterbot.py` sono dei bot di esempio, fatti semplicemente per testare `forza5.py`
e per illustrare come funziona il protocollo di comunicazione tra bot e server.

## Utilizzo
Per disputare una partita tra due bot, basta eseguire `forza5.py` passando come argomenti
i due bot. Ad esempio

`./forza5.py ./clusterbot.py ./clusterbot.py`

Per ulteriori opzioni vedere `./forza5.py -h`.

## Regole del gioco
Forza 5 si gioca su una griglia *NxN* (originariamente doveva essere infinita, ma siamo pigri), con *N*
di default uguale a 64. Due giocatori, a turno, mettono una pedina del proprio colore in una delle
caselle libere della griglia. Al contrario che in forza 4, non c'è gravità.
Il primo giocatore che riesce a disporre 5 delle proprie pedine in fila orizzontalmente,
verticalmente o diagonalmente, vince. Se nessuno dei due giocatori vince prima che la griglia venga
riempita completamente, la partita finisce in parità. Essenzialmente il gioco è tris ma su una griglia
più grande e mettendo in fila 5 pedine invece che 3.

## Protocollo
Un bot per Forza 5 comunica col server tramite `stdin` e `stdout`. Ad ogni turno del bot, il server scrive
una riga sullo `stdin` del bot, contenente *2N* numeri interi senza segno, scritti in base 10 e separati
da spazi. I primi *N* di questi numeri rappresentano le pedine del bot, gli ultimi *N* rappresentano le
pedine del bot avversario, nel seguente modo:
l'*r*-esimo numero ha il *c*-esimo bit meno significativo uguale a 1 se e solo se c'è una pedina nell'
intersezione tra la *r*-esima riga dall'alto e la *c*-esima colonna da sinistra (tutti gli indici partono da 0)

Il bot deve rispondere scrivendo sul suo `stdout` una riga, contenente due numeri separati da spazio,
che sono la riga e la colonna in cui vuole giocare la propria pedina (anche qui, si parte da
in alto a sinistra con la casella (0, 0))

