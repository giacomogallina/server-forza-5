#!/usr/bin/env python3
import random

while True:
    s = input()
    rows = [int(r) for r in s.split()]
    N = len(rows)//2
    board = [rows[:N], rows[N:]]

    free_spots = []
    for r in range(N):
        for c in range(N):
            if (board[0][r] | board[1][r]) & (1 << c) == 0:
                free_spots.append((r, c))

    r, c = free_spots[random.randint(0, len(free_spots)-1)]
    print(r, c)
